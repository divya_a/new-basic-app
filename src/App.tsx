import React, { useState } from "react";
import "./App.css";
import { Provider } from "react-redux";
import { store } from "./store/store";
import { MainCounter } from "./components/MainCounter";

function App() {
  return (
    <Provider store={store}>
      <MainCounter />
    </Provider>
  );
}

export default App;
