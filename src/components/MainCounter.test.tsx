import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { screen, render, cleanup, fireEvent } from "@testing-library/react";
import { MainCounter } from "./MainCounter";
import { rootReducer, initialState } from "../store/rootReducer";
const renderWithRedux = (
  component: any,
  { initialState, store = createStore(rootReducer, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{component}</Provider>),
    store
  };
};

afterEach(cleanup);

it("checks initial state is equal to 0", () => {
  const { getByTestId } = renderWithRedux(<MainCounter />);
  expect(getByTestId("counter")).toHaveTextContent("0");
});
it("increments the counter", () => {
  const { getByTestId } = renderWithRedux(<MainCounter />);

  fireEvent.click(screen.getByText("Increment"));
  expect(getByTestId("counter")).toHaveTextContent("1");
});

it("decrements the counter", () => {
  const { getByTestId } = renderWithRedux(<MainCounter />);
  fireEvent.click(screen.getByText("Decrement"));
  expect(getByTestId("counter")).toHaveTextContent("-1");
});
