import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { InitialState } from "../store/rootReducer";
import { RootDispatcher } from "../store/actions";
interface StateProps {
  count: number;
}

export const MainCounter = () => {
  const { count } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        count: state.count
      };
    }
  );
  const dispatch = useDispatch();
  const rootDispatcher = new RootDispatcher(dispatch);
  const increment = () => {
    rootDispatcher.increaseCount(count);
  };

  const decrement = () => {
    rootDispatcher.decreaseCount(count);
  };
  return (
    <>
      <h1 id="counter">{count}</h1>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </>
  );
};
