import { Dispatch } from "redux";
import { DispatchAction, ActionType } from "./rootReducer";

export class RootDispatcher {
  private readonly dispatch: Dispatch<DispatchAction>;

  constructor(dispatch: Dispatch<DispatchAction>) {
    this.dispatch = dispatch;
  }
  increaseCount = () => this.dispatch({ type: ActionType.INCREASE_COUNT });
  decreaseCount = () => this.dispatch({ type: ActionType.DECREASE_COUNT });
}
