import { rootReducer, ActionType } from "./rootReducer";

describe("counter reducer", () => {
  it("should handle Increase count", () => {
    expect(
      rootReducer(
        { count: 0 },
        {
          type: ActionType.INCREASE_COUNT
        }
      )
    ).toEqual({
      count: 1
    });
  });
  it("should handle decrease count", () => {
    expect(
      rootReducer(
        { count: 0 },
        {
          type: ActionType.INCREASE_COUNT
        }
      )
    ).toEqual({
      count: -1
    });
  });
});
