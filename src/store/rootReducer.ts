import { Action, Reducer } from "redux";

export enum ActionType {
  INCREASE_COUNT = "INCREASE_COUNT",
  DECREASE_COUNT = "DECREASE_COUNT"
}

export interface InitialState {
  count: number;
}

export const initialState: InitialState = {
  count: 0
};

export interface DispatchAction extends Action<ActionType> {
  payload?: Partial<InitialState>;
}

export const rootReducer: Reducer<InitialState, DispatchAction> = (
  state = initialState,
  action
) => {
  console.log(state);
  if (action.type === ActionType.INCREASE_COUNT) {
    return { count: state.count + 1 };
  }
  if (action.type === ActionType.DECREASE_COUNT) {
    return { count: state.count - 1 };
  } else return state;
};
